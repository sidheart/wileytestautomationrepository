package core;

public enum ReportStatus
{
	PASS, FAIL, WARN
}
