package pageobjects;

import java.time.Duration;
import java.util.NoSuchElementException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageBase
{
	protected WebDriver driver;
	
	public PageBase(WebDriver driver)
	{
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
	
	protected WebElement waitForElement(WebDriver driver,WebElement ele,int timeout)
	{
		return new WebDriverWait(driver,timeout).ignoring(NoSuchElementException.class).pollingEvery(Duration.ofSeconds(5)).until(ExpectedConditions.elementToBeClickable(ele));
	}
}