package util;

import java.util.HashMap;
import java.util.Map;

import core.XL_ReadWrite;

// Read Test Data
public class Get_XL_Data
{
	private XL_ReadWrite xl;
	
	// Read data from excel sheet
	public Map<Integer,HashMap<String,String>> Read_Data(String Module, String TestCase)
	{
		int tcPointer;
		int tcMaxCol;
		Map<Integer,HashMap<String,String>> finalData;
		HashMap<String,String> itrDataSet;
		try
		{
			xl = new XL_ReadWrite(System.getProperty("user.dir")+"//DataSource//TestData.xlsx");
			
			// Get the test case pointer
			for(tcPointer=0; tcPointer<=xl.getRowCount(Module); tcPointer++)
			{
				if(xl.getCellData(Module, tcPointer, xl.getColNum(Module, 0, "TestCase")).equals(TestCase))
				{
					break;
				}
			}
			// Get Column Number
			tcMaxCol = xl.getColCount(tcPointer,Module);
			finalData = new HashMap<Integer,HashMap<String,String>>();
			for(int iRow = tcPointer+1;iRow <= xl.getRowCount(Module); iRow++ )
			{	
				itrDataSet = new HashMap<String,String>();
				for(int iCol=0;iCol<=tcMaxCol;iCol++)
				{
					if(iCol > 1) itrDataSet.put(xl.getCellData(Module, tcPointer, iCol), xl.getCellData(Module, iRow, iCol));
				}	
				itrDataSet.remove("");
				finalData.put(Integer.parseInt(xl.getCellData(Module, iRow,1)), itrDataSet);
				if(xl.getCellData(Module, iRow+1,0).isEmpty())
				{
					return finalData;
				}
			}
		}
		catch (Exception e)
		{
			System.out.println(e.getMessage());
		}
		return null;
	}
}
