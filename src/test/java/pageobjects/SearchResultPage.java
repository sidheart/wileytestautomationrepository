package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchResultPage extends PageBase
{	
	@FindBy(xpath=".//div[@class='search__result search__result--space']")
	private WebElement searchedResult;
	
	@FindBy(xpath="(.//img[@alt='Wiley Online Library'])[1]")
	private WebElement homeImg;
	
	public SearchResultPage(WebDriver driver)
	{
		super(driver);
	}
	
	public boolean verifyResult(String expectedText) {
		try {
			return waitForElement(driver,searchedResult,2).getText().contains(expectedText);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	
	public void navigateToHome()
	{
		try
		{
			homeImg.click();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
}
