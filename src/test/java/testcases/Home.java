package testcases;

import java.util.HashMap;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import core.ReportStatus;
import core.TestBase;
import pageobjects.HomePage;
import pageobjects.SearchResultPage;

// Name of the class where test case is resided should be same as corresponding xlsx sheet tab.
// This class name was changed from TestHome.java to Home.java as per first tab in xlsx sheet.

public class Home extends TestBase
{
	private HomePage home;
	private SearchResultPage result;
	
	@BeforeClass
	public void initTest()
	{
		home = new HomePage(driver);
		home.launchApp(getProperties("URL_"+getProperties("ENV")));
	}
	
	@Test(dataProvider = "testdata")
	public void Verify_Search_Title(int itr,HashMap<String,String> td)
	{
		//home = new HomePage(driver);
		
		if(home.isHomePage())
		{
			report.log(ReportStatus.PASS,"Home Page launched");
		}
		else
		{
			report.log(ReportStatus.FAIL,"Unable to access Home page");
		}
		
		result = home.performSearch(td.get("SearchTitle"));
		
		if(result.verifyResult(td.get("SearchTitle")))
		{
			report.log(ReportStatus.PASS,"Searched result verified successfully for "+td.get("SearchTitle"));
		}
		else
		{
			report.log(ReportStatus.FAIL,"Searched result didnt find the word"+td.get("SearchTitle") );
		}
		result.navigateToHome();
	}
}
